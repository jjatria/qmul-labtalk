<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title>Explaining and using ultrasound technology in phonetics research</title>

    <link rel="stylesheet" href="css/reveal.css">
    <link rel="stylesheet" href="css/theme/beige.css">
    <link rel="stylesheet" href="css/theme/jjatria.css">

    <!-- Theme used for syntax highlighting of code -->
    <link rel="stylesheet" href="lib/css/zenburn.css">

    <!-- Printing and PDF exports -->
    <script>
      var link = document.createElement( 'link' );
      link.rel = 'stylesheet';
      link.type = 'text/css';
      link.href = window.location.search.match( /print-pdf/gi ) ? 'css/print/pdf.css' : 'css/print/paper.css';
      document.getElementsByTagName( 'head' )[0].appendChild( link );
    </script>
  </head>
  <body>
    <div class="reveal">
      <div class="slides">

        <section data-markdown>
          <script type='text/template'>
            ## Explaining and using ultrasound technology in phonetics research

            ##### José Joaquín Atria

            21 October, 2016 — QMUL
          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ## Articulatory Phonetics

            * Studies *how we make* speech sounds

            * An important part of Phonetics

            * But how do we look into our mouths while speaking?

            <aside class="notes">
              Phonetics studies the sounds of language, but this is a complex object, that can be grasped from many different perspecives.
              Articulartory Phonetics is the area of Phonetics that studies how the sounds of speech are made.
              This is an important part of Phonetics, since it tells us important things about the interface between our bodies and speech, provides insights into speech itself, and has signficant therapeutic and clinical applications, as well as applications in language teaching, etc.
              The main issue is a methodological one: how do we look into the mouth of a speaking person without interfering with their speech?
            </aside>
          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ## A suitable method

            The method needs to be

            * As cheap as possible

            * Real-time

            * Non-invasive

            * Applicable to many

            <aside class="notes">
              Whatever method is used, it needs to fulfill a certain number of requirements.
              Apart from the obvious (rigorous, reproducible, etc), it should be economically feasible, it should provide images in real-time, it should be non-invasie, and should be applicable to as many people as possible.
            </aside>
          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ## Ultrasound to the rescue

            <figure>
            ![A Spanish trill](images/spanish_trill.gif)
            <figcaption>
            A Spanish trill
            </figcaption>
            </figure>

            <aside class="notes">
              This is what explains that ultrasound has become so popular in Phonetic Research: it ticks many of those boxes, and provides a very good trade-off between image quality, cost, and ease of deployment.
            </aside>
          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ## A convenient method

            * Relative cheap

            * Real-time

            * Non-invasive

            * Non-radioactive

            <aside class="notes">
              Ultrasound is also a convenient method, and one that we are at least partially familiar with, since it is so commonly used with pregnant women.
              The reason it is so convenient is that it provides a cheap and real-time, non-invasive window into our bodies.
              And unlike other means to see into our bodies (like eg. X-rays), ultrasound is good because it also emits no radiation.
            </aside>
          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ## Safe to use

            Since it is not radioactive, it is safe to use

            * On unborn children

            * In areas close to the brain, like the neck and eyes

            * Including inside our mouths!

            <aside class="notes">
              This means it can be safely used in delicate areas, like inside the wombs of pregnant women.
              But also in areas close to the brain, like the neck and eyes... and inside our mouths.
              This allows us to look at how we make the sounds of speech with minimal interference.
            </aside>
          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ## How does it work?

            * Sound travels through a medium

            * Speed depends on density:

            solid > liquid > gas

            <aside class="notes">
              As its name implies, ultrasound uses sound waves.
              Although the waves have a very high frequency, they are still just sound waves, and like all sound waves, they need some medium to travel.
              Sound normally travels through the air, which is a gas; but it can travel through all sorts of substances, including liquids and solids.
              The speed at which it does so will depend on the density of that substance: the harder the substance, the faster sound will travel.
            </aside>
          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ## Changes in density

            * When the density changes, so does the speed

            * ... and some energy dissipates!

            * Some of that energy bounces back

            * How much?

                * More if density increases

                * Less if density decreases

            <aside class="notes">
              One of the properties of sound is that, when the density of the medium changes --- for example, when traveling through the air it bumps into a wall --- some of the sound energy will dissipate.
              Part of it will continue its path, in this case into the wall, and the rest will dissipate into the environment, part of it bouncing back in my direction.
              The amount of energy that is absorbed by the new object depends on a number of properties, but it basically depends on the objects density, such that denser objects will reflect more energy back.
            </aside>
          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ## Locating objects

            * Direction: the **source** of the bounce

            * Distance: the **delay** of the bounce

            * Density: the **intensity** of the bounce

            This is how sonar and medical ultrasound work!

            <aside class="notes">
              This already allows us to use sounds to locate objects in space.
              We know the direction at which the object is because we know where the sound bounced from.
              And knowing the medium through which the sound was traveling, we also know its speed, which means we can look at the time it took to come back to infer the distance of the object.
              Finally, from the amount of energy that bounced back, we also can tell the density of this object.
              This is how sonar works in both bats and submarines, and how medical ultrasound allows us to see children before they are born.
            </aside>
          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ## Ultrasound Tongue Imaging

            * Visibility is good, but limited

            * Main visible feature is tongue surface

            * Probe is fixed under the chin

            * Important to have reference points

            <aside class="notes">
              Despite allowing us to see inside the mouth, not everything is visible.
              Indeed, the main visible feature is the tongue surface, which is the first point at which the density changes.
              This is why this method is more appropriately called "Ultrasound Tongue Imaging".
              When used for Phonetic research, the ultrasound probe is placed under the chin, next to the throat, and fixed to a headset pointing up.
              This is so that the probe, which is otherwise hand-held, stays fixed, providing a way to make comparisons between the frames of the video.
              But unless other points of reference are found, this will still just be an image of a tongue surface floating in space.
            </aside>
          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ## Finding the palate

            * Hard palate can be a reference point

            * But it is almost entirely invisible!

                * Isolated by an air gap

            * Can be made visible by

                * Bringing tongue in contact (swallowing)

                * Bridging the gap (with liquid)

            <aside class="notes">
              The hard palate is one obvious choice for a reference point.
              But points beyond the tongue, like the palate, are not visible because of the air gap that separates the two.
              So in order to see the palate the tongue needs to be in contact with it, or some other medium, like water, must be present to bridge that gap.
              When setting up, the palate can be detected through these means.
              Once located, a spline can be fitted to the palate contour and extended to the other frames, to provide a reference point.
            </aside>
          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ## Finding the teeth

            * The teeth can also be a reference point

            * But they are also hard to see

            * We can use a bite plate

                * Length behind the teeth is known

            <figure>
            ![Biting the plate](images/biting_plate.jpg)
            ![A spline fixed to the plate](images/plate_spline.jpg)
            <figcaption>
            Using a bite plate
            </figcaption>
            </figure>

            <span class="reference">Images © Carnegie Trust: [An online UTI resource for Phonetics, Linguistics and Speech Therapy Teaching](http://www.seeingspeech.ac.uk)</span>

            <aside class="notes">
              Likewise, it can also be useful to know where the tongue is in relation to the teeth.
              But like the palate, they are also largely invisible, in this case because they are hidden by the lower jaw.
              In order to find them, the participant can be asked to bite a plastic plate.
              The plate has a shape such that, after biting, the length of the portion behind the teeth is known.
              This can be used to locate the teeth at the beginning of the session, and marking it with a fitted spline for the rest of the session.
            </aside>
          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ## To wrap up

            * Ultrasound is non-invasive and cheap

            * It makes real-time recordings

            * It provides a clear image of the tongue

            * But you need reference points!

            <aside class="notes">
              Ultrasound has many features that make it a desirable tool for phonetic research.
              It is relatively cheap, non-invasive, and provides good and clear real-time images of the tongue surface without interfering with a participant's speech.
              But in order for the images to be useful, reference points are needed, so it is important to fix the probe to the participant's head, and to find the position of the tongue in relation to other hard reference points in the mouth, such as the palate and the teeth.
            </aside>
          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ## Thank you

            Questions?

            <figure>
            ![A Spanish trill](images/spanish_trill.gif)
            <figcaption>
            </figcaption>
            </figure>
          </script>
        </section>
      </div>
    </div>

    <script src="lib/js/head.min.js"></script>
    <script src="js/reveal.js"></script>

    <script>
      // More info https://github.com/hakimel/reveal.js#configuration
      Reveal.initialize({
        history: true,
        // Bounds for smallest/largest possible scale to apply to content
        minScale: 0.2,
        maxScale: 1.0,
        showNotes: false,

        // More info https://github.com/hakimel/reveal.js#dependencies
        dependencies: [
          { src: 'plugin/markdown/marked.js' },
          { src: 'plugin/markdown/markdown.js' },
          { src: 'plugin/notes/notes.js', async: true },
          { src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } }
        ]
      });
    </script>
  </body>
</html>
